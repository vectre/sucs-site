<?php
// Generate the list of who's logged in
$users = $DB->GetAll("SELECT DISTINCT username, min(date_trunc('second', now()-time)) AS idle FROM session WHERE username IS NOT NULL group by username order by idle LIMIT 5;");

$smarty->assign('users', $users);

$result = $smarty->fetch('users.tpl');
?>
