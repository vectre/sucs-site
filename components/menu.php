<?php
function getPageID($name)
{
    global $DB;
    $query = $DB->GetRow("select id from menu where title='" . $name . "'");
    return (int)@$query['id'];
}

function translate($word)
{
    global $language, $DB;

    if ($language['code'] != "en") {
        $query = $DB->GetRow("select title, title" . $language['db'] . " from menu where title='" . $word . "'");
        if ($query['title' . $language['db']] != "") return $query['title' . $language['db']];
        else return $query['title'];
    } else return $word;
}

function parseMenu($res)
{
    global $language;

    $menu = array();

    foreach ($res as $row) {
        if ($language['code'] != "en") {
            if ($row['title' . $language['db']] != "") $title = $row['title' . $language['db']];
            else $title = $row['title'];
            $menu[$title] = $row['target'];
        } else {
            $menu[$row['title']] = $row['target'];
        }
    }

    return $menu;
}


if (isset($pathlist[1]))
    $pagename = $pathlist[1]; else $pagename = "";

// Menu stuff
$smarty->assign("select", $pagename);

if (isset($pathlist[2]))
    $smarty->assign("subselect", $pathlist[2]);
else
    $smarty->assign("subselect", $pagename);

$query = "select * from menu where parent is NULL and (permission is NULL";

// Temporary - all members should be members of the "users" group but aren't
if ($session->loggedin) $query .= " or permission='users'";

foreach ($session->groups as $group => $value) {
    $query .= " or permission='$group'";
}

$query .= ") order by menuorder";

$res = $DB->GetAll($query);
$menu = parseMenu($res);

// this needs to choose the actual current one
// subpages/submenu items can have permissions attached to them as well!

$query2 = "select * from menu where parent=";
$query2 .= "'";
$query2 .= getPageID($pagename);
$query2 .= "'";
$query2 .= " and (permission is NULL";

if ($session->loggedin) $query2 .= " or permission='users'";

foreach ($session->groups as $group => $value) {
    $query2 .= " or permission='$group'";
}

$query2 .= ") order by menuorder";

$res2 = $DB->GetAll($query2);
if (count($res2) > 0) {
    $submenu = parseMenu($res2);
    $menu[translate($pagename)] = $submenu;
}

$smarty->assign("menu", $menu);
?>
