<p>
<script type="text/javascript"><!--
//<![CDATA[
        function popup(mylink, width, height, windowname)
        {
            if (! window.focus) return true;
            var href;
            if (typeof(mylink) == 'string')
                href=mylink;
            else
                href=mylink.href;
                window.open(href,windowname, 'width='+width+',height='+height+',left=5,top=50,scrollbars=no');
                return false;
        }
// --></script>
</p>
<p>The Desktop on Demand service allows you to connect to an X session on one of the SUCS desktops and use all the programs you can in the SUCS room. Please remember that the <a href="{$baseurl}/About/Conditions">SUCS Terms and Conditions</a> apply to your use of this service.</p>
<p>Choose a desktop size to start using the service (smaller will be faster):</p>
<ul>
<li><a href="/desktop/640x480.html" onclick="return popup('/desktop/640x480.html','640','480','Desktop on Demand');">640x480</a></li>
<li><a href="/desktop/800x600.html" onclick="return popup('/desktop/800x600.html','800','600','Desktop on Demand');">800x600</a></li>
<li><a href="/desktop/1024x768.html" onclick="return popup('/desktop/1024x768.html','1024','768','Desktop on Demand');">1024x768</a></li>
</ul>