<div class="box">
<div class="boxhead">
<h2>Jon Gordon (itsme) - "How a SCRAMjet Works"</h2>
</div>
<div class="boxcontent">
<p>An overview of the workings of a SCRAMjet.</p>
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2011-11-01/itsme.flv" height="275" id="player" type="application/x-shockwave-flash" width="320">
<param name="height" value="256" />
<param name="width" value="320" />
<param name="file" value="/videos/talks/2011-11-01/itsme.flv" />
<param name="image" value="/videos/talks/2011-11-01/itsme.png" />
<param name="id" value="player" />
<param name="displayheight" value="256" />
<param name="FlashVars" value="image=/videos/talks/2011-11-01/itsme.png" />
</object>
</div>
<p><strong>Length: </strong>12m 45s</p>
<p><strong>Video: </strong><a href="/videos/talks/2011-11-01/itsme.ogv" title="720x576 Ogg
Theora - 23MB">720x576</a> (Ogg Theora, 23MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
