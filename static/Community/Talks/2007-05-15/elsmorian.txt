<div class="box">
	<div class="boxhead"><h2>Chris Elsmore (elsmorian) - Semiconductor &amp; Microprocessor Technology</h2></div>
	<div class="boxcontent">
	<p>Chris Elsmore gives an in-depth look at the technology behind microprocessors, and looks at the past, present, and what's yet to come.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-elsmorian.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-elsmorian.flv" image="/videos/talks/2007-05-15/2007-05-15-elsmorian.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-elsmorian.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-elsmorian.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-elsmorian.png" />
</object></div>

<p><strong>Length: </strong>13m 21s</p>
<p><strong>Video: </strong><a href="/videos/talks/2007-05-15/2007-05-15-elsmorian.mov" title="784x576 H.264 .mov - 98.3MB">784x576</a> (H.264 .mov, 98.3MB)</p>
<p><strong>Slides:</strong><a href="/videos/talks/2007-05-15/2007-05-15-elsmorian-slides.pdf" title="Semiconductor &amp; Microprocessor Technology">2007-05-15-elsmorian-slides.pdf</a> (PDF, 700KB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
