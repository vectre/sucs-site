<p>SUCS doesn't have any hard limits on the amount of disk space that it's members may use. We also allow our members free printing - within reason.<br />These pie charts identify who is hogging the most disk space, and who has destroyed the most forests. Click the icons for more detail.</p>
<p>&nbsp;</p>
<div style="text-align: center">
<span style="padding-right: 15px;">
<a href="Shame/Disk">
<img alt="Top 12 Disk Space Users" height="158" src="../images/hard_drive.png" width="172" />
</a>
</span>
<span style="padding-left: 15px;">
<a href="Shame/Printer">
<img alt="Top 12 Printers" height="146" src="../images/printer.png" width="187" />
</a>
</span>
</div>
<p>&nbsp;</p>