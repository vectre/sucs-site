




<p>Thunderbird is the mail client from Mozilla. You can install it from your <span style="font-style: italic">SUCS Member&#39;s CD</span> or download it from <a href="http://www.mozilla.com/thunderbird/">mozilla.org</a>.</p>

When you run Thunderbird for the first time, it will ask you to set up an account.<br />
<img src="../../../../pictures/tbnewacc.png" border="0" alt="tbnewacc.png" hspace="0" vspace="0" width="326" height="322" align="bottom" /><br />
Make sure that &quot;Email account&quot; is selected, then click <span style="font-weight: bold">Next</span>.<br />
<br />
Enter your name and your email address. Your email address will be your
SUCS username followed by &quot;@sucs.org&quot;. When you have done this, click <span style="font-weight: bold">Next</span>.<br />
<br />
<img src="../../../../pictures/tbserver.png" border="0" alt="tbserver.png" hspace="0" vspace="0" width="326" height="322" align="bottom" /><br />
You will be asked to choose between POP and IMAP mail collection. SUCS
supports both, but IMAP is generally better so choose that. Put <span style="font-style: italic">sucs.org</span>
into the boxes for both the Incoming and Outgoing servers. Note that
these settings are not complete. You will need to use secure IMAP, and
that option is not available in the wizard. When we have completed the
wizard, we will need to go back and select this option. Click <span style="font-weight: bold">Next</span>.<br />
<br />
You will now be asked for your username. Thunderbird will guess that
this is the bit before the @ sign in your email address. In this case
it is right, so click <span style="font-weight: bold">Next</span>.<br />
<br />
Now, you need to choose a name for your account. The default of your
email address is fine, but you could call it &quot;SUCS Account&quot;, for
example, if you prefer. Click <span style="font-weight: bold">Next</span>.<br /><br /><img src="../../../../pictures/tbwizardfinish.png" alt="tbwizardfinish.png" /><br />You have now completed the wizard - click <span style="font-weight: bold">Finish</span>.<br /><br /><img src="../../../../pictures/tbserversettings.png" border="0" alt="tbserversettings.png" hspace="0" vspace="0" width="446" height="370" align="bottom" /><br />Now go to the <span style="font-style: italic">Tools</span> menu and select <span style="font-style: italic">Account Settings...</span> Click on the <span style="font-style: italic">Server Settings</span> section on the left and tick <span style="font-style: italic">Use secure connection (SSL)</span>.<br /><br /><img src="../../../../pictures/tbsmtp.png" alt="tbsmtp.png" /><br />Now click on <span style="font-style: italic">Outgoing Server (SMTP)</span>. Here, you should select <span style="font-style: italic">TLS</span>. Click <span style="font-weight: bold">OK</span>. You have now completed the configuration of Thunderbird.<br />

