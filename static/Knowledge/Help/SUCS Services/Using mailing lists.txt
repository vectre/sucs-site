<p>When you join a society that is a member of SUCS, you will quite likely have given them your email address so they can email you. The system SUCS provides to societies to send email to their members is called <em>mailman</em>. Here is a brief guide to using mailman&#39;s web interface to change your preferences, using the mailing list <em>jokes</em> as an example. You can apply these principles to other mailing lists that you may be (or want to be) subscribed to.</p>  

<p>Note: there are two general methods for using mailing lists: the web interface and the email interface. To get help on the email interface, send an email to (for example) <a href="mailto:jokes-request@lists.sucs.org" title="jokes-request@lists.sucs.org">jokes-request@lists.sucs.org</a> with the word &#39;help&#39; in the subject or body.</p>  

<h3>Subscribing to lists</h3>  
<p>To subscribe to the <em>jokes</em> mailing list, go to <a href="http://lists.sucs.org/mailman/listinfo/jokes" target="_blank" title="http://lists.sucs.org/mailman/listinfo/jokes">http://lists.sucs.org/mailman/listinfo/jokes</a>. On this page is a description of the list, a form for subscribing, and (for existing members) forms for seeing the members list and changing your list options. Enter the email address you want mail from the list to be sent to. If you want, you can supply your name (so people looking at the members list know who you are), and you can pick a password. If you don&#39;t pick a password, mailman will pick one for you, and email it to you.</p>  

<p><span style="color: red">WARNING</span>: Do not use an important password for the list, because it will be emailed to you periodically as a reminder, in clear text.</p>  

<p>You can also subscribe by sending an email to (e.g.) <a href="mailto:jokes-subscribe@lists.sucs.org" title="jokes-subscribe@lists.sucs.org">jokes-subscribe@lists.sucs.org</a>. You will be sent back an email asking you to confirm you want to subscribe. Simply reply to the email, keeping the subject intact (though a Re: at the start is OK), or type the URL (web address) it gives you into a web browser.</p>  

<h3>Changing your preferences</h3>  
<p>Mailman has lots of options for things like digest mode (where you receive all the day&#39;s email in one big &#39;digest&#39; instead of as individual emails - this is particularly handy if the list sees a lot of traffic) and whether you want to receive copies of your own emails to the list. To change these preferences, enter your email address in the box at the very bottom of the list&#39;s info page (<a href="http://lists.sucs.org/mailman/listinfo/jokes" title="http://lists.sucs.org/mailman/listinfo/jokes">http://lists.sucs.org/mailman/listinfo/jokes</a> for the <em>jokes</em> list) and click on the button next to it. This sends you to a page with lots of options that you can leave alone or change as you wish.</p>  

<h3>Sending messages to the list</h3>  
<p>You can send a message to everyone subscribed to the list simply by sending an email from the address you subscribed with to (e.g.) <a href="mailto:jokes@lists.sucs.org" title="jokes@lists.sucs.org">jokes@lists.sucs.org</a>.</p>  

<p>Note: Some mailing lists are &#39;announcement&#39; lists (usually called something like <em>jokes-announce</em>).  You can still send mail to these lists, but they have to be approved by a <em>moderator</em>, a person who decides if an email is suitable for the list or not.</p>  

<h3>Unsubscribing from lists</h3>  
<p>To unsubscribe from a list you don&#39;t want to get mail from (or send mail to) anymore, log in to your preferences page for that list. On the following page there is a button you can click on to unsubscribe. It&#39;s easy to click on this by mistake, so the form makes you click on the checkbox below it first.</p>  

<p>Alternatively, you can send mail to (e.g.) <a href="mailto:jokes-unsubscribe@lists.sucs.org" title="jokes-unsubscribe@lists.sucs.org">jokes-unsubscribe@lists.sucs.org</a>. You will be sent back an email asking you to confirm you want to unsubscribe. Simply reply to the email, keeping the subject intact (though a Re: at the start is OK), or type the URL (web address) it gives you into a web browser.</p>
