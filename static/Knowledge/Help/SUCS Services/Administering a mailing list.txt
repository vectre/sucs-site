<p>Having a mailing list allows your society to communicate easily with
its members. If your society has a SUCS account and would like to have
a mailing list, just send an email to <a href="mailto:admin%20at%20sucs.org" title="admin at sucs.org">admin at sucs.org</a> asking for one, and we will set it up, using your SUCS email as the list administrator address.</p>
<h3>Overview</h3>
<p>There are two types of mailing list:</p>
<ul>
<li>Discussion forums</li>
<li>Announcements</li>
</ul>
<p>You will need to tell us which of these you want when you ask us to create the list.</p>
<p>A discussion forum is a mailing list with which anyone subscribed to
the list can send messages to everyone else subscribed to the list. As soon as a
member sends an email to the list address (e.g. funsoc at lists.sucs.org
might be the Fun Society's mailing list address) it will be forwarded
to everyone who is subscribed to the list (except those using digest
mode, to whom it sends one email a day, containing all that day's
emails).</p>
<p>An announcements list is <span style="font-style: italic;">moderated</span>,
which means subscribers can still send messages to it, but these
messages are not immediately sent to the entire list, but are instead
held by the system for approval by a <span style="font-style: italic;">moderator</span>.
All the list administrators are also moderators, but you can additionally allow
non-administrators to be moderators. By convention these lists are
called something like <span style="font-style: italic;">funsoc-announce</span>.</p>
<p>Most administrative tasks can be done by visiting (e.g.) http://lists.sucs.org/mailman/admin/funsoc. At the top of this page is a menu with links to pages containing different categories of options.</p>
<h3>Adding users</h3>
<p>You can add a large number of addresses to the list at once by clicking on <span style="font-style: italic;">Membership Management</span> then <span style="font-style: italic;">Mass Subscription</span>. Do one of the following:</p>
<ul>
<li>Type (or paste) into the first large box the addresses you wish to add, each on a separate line; or</li>
<li>Put the addresses in a file, each on a separate line, and type this file's name into
the smaller box. The button beside it gives you an easy, graphical way
to find the file.</li>
</ul>
<p>Don't try to use both mechanisms, because it won't work.</p>
<p>The second large box can be filled in with a welcome message to send
to each address you are adding to the list. You should finish this
message with a blank line.</p>
<p>You can choose whether the addresses should be subscribed
immediately or just have an invitation sent to them (which allows them
to subscribe simply by replying to the message), and whether the list
admin address gets notified when those members actually subscribe.</p>
<p>Click 'Submit Your Changes' to subscribe the addresses to your list.</p>
<h3>Removing users</h3>
<p>If you find you need to (e.g. at the start of the year when you
purge all ex-members), you can remove a large number of addresses from
the list at the same time; though in this case you don't get to send
them a custom message, only a standard unsubscription notice.</p>
<p>To do this, click on 'Membership Management' -&gt; 'Mass Removal' and proceed more or less as for adding users.</p>
<h3>Changing the mailing list password</h3>
<p>First of all, I would like to encourage all society executives to
pass on their account details (username, password, the structure of
your website if you have one, and what else you have in your home
directory, and whatever else you can think of) to their new executive
as soon as they take over. Not only is this common sense, it also
allows for some continuity in the management of the society's account.
Not to mention that there is no more annoying thing for a sysadmin than
a user asking to have their password changed because they've forgotten
it.</p>
<p>To change your mailing list admin password, simply click on
'passwords'. This also allows you to change the moderator password,
which is used by moderators to authenticate themselves for the purposes
of moderating an announcements list. The form asks you to enter the
password twice, in case you mistype it - PLEASE don't just copy and
paste! It's recommended you change this password any time someone stops
being a list admin.</p>
<h3>General admin options</h3>
<p>Here are some options you may want to know about:</p>
<ul>
<li>Additional admin email addresses can be added under 'General
options'. Any mail relating to list administration will be sent to
these addresses. You might want to put all your executive's email
addresses here (though if you expect them to be able to do anything as
an admin, remember to tell them the password).</li>
<li>You can make non-administrators into moderators in the same way,
except using a different box. These addresses receive notifications of
posts by non-members and (for announcements lists) non-moderators.
Administrators are already moderators so they receive these emails
anyway. Again, if you expect the owners of these addresses actually to
be able to act as moderators, be sure to give them the moderator
password.</li>
<li>There are fields labelled 'description' and 'info' which you can use to describe the list. 'Description' should be brief, such as "Mailing list for Fun Society members". You can put a more detailed description (including HTML markup if you like) in the 'info' field, which is displayed at the top of the list info page.</li>
<li>If there is a discussion on your list that you find is degenerating into a <a href="http://www.catb.org/esr/jargon/html/F/flame-war.html" title="Definition of 'flame war'">flame war</a>, you can turn on emergency moderation to temporarily moderate everything posted by non-moderators.</li>
<li>Some societies (e.g. the German Society) might want to receive system messages in a language other than English. You can arrange this on the 'language options' page.</li>
<li>You can choose whether users need to confirm their subscription and if an admin must approve their membership first. You cannot disable both of these; this is to stop malicious people subscribing others to high volume lists against their will. This is under 'Privacy Options' -&gt; 'Subscription Rules'. Should you ever need to, you can also ban certain addresses from the list on this page.</li>
</ul>