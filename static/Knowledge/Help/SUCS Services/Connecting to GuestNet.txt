<p>SUCS offers members the opportunity to connect their own computers to our network, providing a connection to the Internet via our firewall. In order for you to use your computer on our network, you may need to make some configuration changes to your network setup. Once you are connected, just point your web browser at <a href="../../../guestnet">https://sucs.org/guestnet</a> to register.<br /></p>

<h2>Under Windows</h2>
<p>Bring up the properties window for your network card. Select the TCP/IP component for your network card, and click Properties. Ensure that &quot;Obtain an IP address automatically&quot; is selected. Also ensure the DNS Configuration tab is set to get its settings automatically</p>

<h2>Under Linux</h2>
<p>Use what ever tool your distribution provides to setup up your network card to receive its settings via DHCP.</p>

<h2>Under Mac OS X</h2>
<p>Go to the Apple menu and choose &quot;System Preferences&quot;. Double-click on the &quot;Network&quot; icon. Select the &quot;Built-in Ethernet&quot; device from the &quot;Show&quot; drop-down, then go to the TCP/IP tab and make sure that &quot;Configure&quot; is set to &quot;using DHCP&quot;. Also check that all the other boxes on that tab are empty, then click &quot;Apply Now&quot; and close the window.</p>