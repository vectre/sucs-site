



<div style="text-align: left">

<style>
a.wd {behavior: url(#default#AnchorClick);}
</style>


The Computer Society offers you extra disk space and you can use this
from anywhere on the internet via <a href="http://www.webdav.org">WebDAV</a>. This enables you to see your files on the SUCS network as if you were browsing through folders on your own computer.<br /></div><h2>Windows</h2><p>[<span style="font-weight: bold">Note:</span> These may be out of date instructions for WebDAV on Windows. An up-to-date, slap-dash pictorial walkthrough can be found <a href="http://andrewprice.me.uk/weblog/entry/a-shiny-windows-pictorial-walkthrough-for-sucs-members-who-like-pointing-and-clicking" title="Windows XP WebDAV walkthrough at Andrew Price&#39;s blog">here</a>.]</p><p>In an Internet Explorer window the following link should just <a class="wd" href="https://sucs.org/dav" target="_blank" folder="https://sucs.org/dav">Open a Web Folder</a>, if it does not then try the next suggestion.</p><p>In an Explorer window (the one with the file
icons), select <strong>Open</strong>, then enter https://sucs.org/dav/ in
the <strong>Go to</strong> box, tick <strong>Open as web folder</strong> and hit OK.

<br /></p><p>
You will be asked to accept a digital certificate (used to keep the file
transfers between your workstation and the machines encrypted), and then for
your login and password. After this you will get an explorer window showing
the files on your computer society space. </p><h2>Linux</h2><h3>Gnome</h3><p>Open a nautles window, select Open Location from the file menu, and give davs://sucs.org/dav as the location, you will be promted for your SUCS username and password.<br /></p><h3>KDE</h3><p>Anyone that uses KDE feel free to write this bit!<span style="font-weight: bold"><br /></span></p><h2 style="text-align: left">Mac OS X</h2><p>Grab Finder, In the menu navigate to &quot;Go -&gt; Connect to Server&quot; or just key in Command+K then enter https://sucs.org/dav/  as the server address, you will be promted for your SUCS username and password.</p>