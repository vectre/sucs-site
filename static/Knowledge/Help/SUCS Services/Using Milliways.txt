










<p>Milliways is our BBS (think chat room and message board with knobs
on). Milliways is the best place to look to find a member of the admin
team if you&#39;re having problems with any SUCS services. However, it can
be a little confusing for the first time user. Read on and it won&#39;t be
quite so painful (honest!).</p>
<h2>Starting Milliways</h2>
<p>To start Milliways you need to be logged into silver. See <a href="Logging%20in%20remotely">this tutorial</a> to find out how.</p>
<p>You then start Milliways with the command &quot;mw&quot;. Your screen should now look like this:</p>
<pre class="console">silver:~$ mw<br /> <br />                  #     #<br />###  ###   ##    ##    ##    ##   +------------------------+<br /> ##  ##         ###   ###         |   on sucs.swan.ac.uk   |<br /> ### ##          ##    ##         +------------------------+<br /> ######     #    ##    ##     #   ###   ##   ####    ####  ##   ####<br /> ######    ##    ##    ##    ##    ##   #   #   ##    ##   #   ##  #<br /> # # ##   ###    ##    ##   ###    ## # #      ###     ## ##   ###<br /> #   ##    ##    ##    ##    ##    ## ###    ## ##     ####     ###<br /> #   ##    ##    ##    ##    ##    ######   ##  ###     ###      ###<br /> #   ##    ##    ##    ##    ##    ## ##    ## ###       #     #  ##<br />### ####  ####  ####  ####  ####   #  #      ## #       #      ####<br />                                                      ###<br />                                                      ##<br /> <br />Milliways III - Release 2.13<br /> <br />Please enter name:</pre>
<h2>Sign up</h2>
<p>The first time you start Milliways you have to sign up. This means
you&#39;ll have to enter, user name (should be your SUCS user name), a
password of your choice, your real name, and an contact email address.
This information is required in addition to the information you
provided when you joined SUCS as Milliways is open to everyone, and
therefore does not use the members database.</p>
<p>Once you have registered you&#39;ll be deposited in the message board.</p>

<pre class="console">Please enter name: yourusername<br />Enter Password<br /></pre>
<p> And so on.</p>
<pre class="console">Hello yourusername.<br /> <br />Last logged out Thu Aug 19 20:27:55 2004<br /> <br />{Notices}---*</pre>

<h2>Enter the talker</h2>

<p>Once you&#39;re logged into Milliways, you enter the talker by typing &quot;talk&quot;:</p>
<pre class="console">{Notices}---* talk<br />                                                                                                               <br />Now entering the Talker. Use &quot;.quit&quot; to leave and &quot;.help&quot; for help.<br />talk{0}-*<br /></pre>

<h2>Saying something</h2>
<p>Once you&#39;re in the talker, you can type a line and press Enter to send it.</p>

<pre class="console">talk{0}-* i like pie [enter]</pre>
<p>produces</p>
<pre class="console">yourusername: i like pie</pre>

<h2>Leave the talker</h2>
<p>To leave the talker type:</p>
<pre class="console">talk{0}-* !quit<br />silver:~$<br /></pre>
<h2>Basic Commands</h2>
<p>Now you can enter and leave, it&#39;s time to expand your repertoire.</p>
<p>The two most important commands in Milliways are <span style="font-weight: bold">!help</span> for system
and message board commands and<span style="font-weight: bold"> .help</span> for talker commands. These will
explain in much more detail than I will what all of the available
commands are and what they do.</p>

<pre style="font-weight: bold">.who</pre>

<p>Displays a list of &quot;who&quot; is currently using Milliways, and what room they are in.</p>
<pre style="font-weight: bold">!since</pre>

<p>Displays a list of who has been using Milliways &quot;since&quot; you were last on.</p>
<pre><span style="font-weight: bold">.room </span>[number]</pre>
<p>Changes what room you&#39;re in.</p>
<pre><span style="font-weight: bold">!beep</span> on/off</pre>
<p>Turn message beeps on or off.</p>
<pre style="font-weight: bold">.emote</pre>
<p>(can be shortened to .e) Send emoted messages to people; for example:</p>
<pre class="console">.e can smell something</pre>
<p>produces:</p>
<pre class="console">rollercow can smell something</pre>
<p>You can also use:</p>
<pre class="console">.e&#39;s feet smell</pre>
<p>which produces</p>
<pre class="console">rollercow&#39;s feet smell</pre>
<pre style="font-weight: bold">.sayto</pre>
<p>(can be shortened to .sa) Say something to a single user; for example:</p>
<pre class="console">.sa vortex hello!</pre>
<p>would show up as the following message on vortex&#39;s Milliways screen:</p>
<pre class="console">rollercow says: hello!</pre>
<p>.sayto also does questions, by adding a ? to the end of the line; for example:</p>
<pre class="console">.sa vortex where is the next lecture?</pre>
<p>appears on vortex&#39;s Milliways screen as:</p>
<pre class="console">rollercow asks: where is the next lecture?<br /></pre><p>Have a quick look at the <a href="https://sucs.org/Knowledge/Help/SUCS%20Services/Using%20Milliways/Milliways%20Etiquette">Milliways Etiquette</a> page for some simple dos and don&#39;ts to help you survive Milliways. </p>