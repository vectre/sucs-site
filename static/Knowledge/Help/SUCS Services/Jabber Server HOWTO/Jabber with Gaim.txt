<p>In order to connect to and use the Jabber server, you have to register your account. This is easy enough to do in Pidgin: click 'Accounts' from the 'Tools' menu, and then click the button labelled 'Add'. You'll be presented with a window which you should fill in a similar fashion to the following (your Jabber 'Screen Name' should be the same as your SUCS username):</p>
<p>
<img src="/pictures/jabber1.png" /></p>
<p>Click 'register' at this point to register your user.</p>
<p>A new window will appear, which you should fill out similar to this:</p>
<p>
<img src="/pictures/jabber2.png" /></p>
<p>Click 'Register' and, all being well, you'll get a message saying registration succeeded.</p>
<p>To actually connect to the Jabber server, tick the 'Online' box in the Pidgin accounts window. If 'Auto-login' is ticked, you'll be logged in to the Jabber server every time you start Pidgin.</p>
<h3>Adding Buddies</h3>
<p>You'll now want to fill up your buddy list (or 'roster') with people to talk to, so select 'Add Buddy' from the 'Buddies' menu, and enter the buddy's details in the new window:</p>
<p>
<img src="/pictures/jabber3.png" /></p>
<p>This will send an authorisation request to your buddy, who will have to accept it before he/she'll appear on your buddy list.</p>
<p>Once you've got people on your buddy list, just double-click their entry on the list to start chatting.</p>