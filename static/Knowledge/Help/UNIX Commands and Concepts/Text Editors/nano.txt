<p>You launch nano with the command:</p>
<pre class="console">$ nano <em>[filename]</em></pre>
<p>When you do, you will be presented with the following screen:</p>
<pre class="console"><span class="reverse">  GNU nano 1.2.1                  New Buffer                                    </span>




















                                  <span class="reverse">[ New File ]</span>
<span class="reverse">^G</span> Get Help  <span class="reverse">^O</span> WriteOut  <span class="reverse">^R</span> Read File <span class="reverse">^Y</span> Prev Page <span class="reverse">^K</span> Cut Text  <span class="reverse">^C</span> Cur Pos<br /><span class="reverse">^X</span> Exit      <span class="reverse">^J</span> Justify   <span class="reverse">^W</span> Where Is  <span class="reverse">^V</span> Next Page <span class="reverse">^U</span> UnCut Txt <span class="reverse">^T</span> To Spell<br /></pre>
<p>You will notice a number of commands at the bottom of the screen. These are executed by using the key combination to the left of the command name. In this context, the symbol "^" means Ctrl. So to <strong>Get Help</strong>, you would press Ctrl-G; to <strong>Exit</strong>, you would press Ctrl-X, and so on.</p>
<p>Some of the standard <a href="https://sucs.org/Knowledge/Help/UNIX%20Commands%20and%20Concepts/Control%20Keys">control keys</a> apply here.</p>
<p>The official website for nano, which includes documentation, is <a href="http://www.nano-editor.org/">http://www.nano-editor.org/</a>.</p>