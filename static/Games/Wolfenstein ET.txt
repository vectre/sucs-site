<p><cite>Wolfenstein: Enemy Territory</cite> is a freely available multiplayer FPS game.</p>
<p><a href="ftp://ftp.sucs.org/linux/games/et-linux-2.60.x86.run">Download the Linux version</a> (x86 only)</p>
<p><a href="ftp://ftp.sucs.org/mac/Wolfenstein_ET260c.dmg">Download the Mac version</a></p>
<p><a href="http://sucs.org/~tswsl1989/ET/WolfET.exe">Download the Windows version</a></p>
<p>To connect to the SUCS Wolfenstein: Enemy Territory server:</p>
<ol>
<li>Choose <em>"Play Online"</em> from the main menu</li>
<li>Click <em>"Connect to IP"</em> button, below the server browser.</li>
<li>Enter&nbsp; <span class="tt">games.sucs.org:27016</span> and click <em>"OK"</em></li>
</ol>