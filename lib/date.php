<?php


// Determine the academic year of a specific timestamp
// Returns the year which this academic year started, e.g. 2006 for the year 2006/07  
function academicYear($timestamp)
{

    $date = getdate($timestamp);

// Anything before September is the previous academic year
    if ($date['mon'] < 9) {
        return $date['year'] - 1;
    } else {
        return $date['year'];
    }
}

function paidUntil($timestamp)
{
    $nextyear = academicYear($timestamp) + 1;
    $paydate = "Sept. " . $nextyear;
    return $paydate;
}

?>
