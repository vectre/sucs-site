<div id="navigationC">
    <ul>
        {foreach name=menu from=$menu key=name item=target}
            {if is_array($target)}
                <li><a href="{$baseurl}/{$name}"{if $select==$name} class="select"{/if}>{$name}</a>
                <ul>
                    {foreach name=submenu from=$target key=subitem item=subname}
                        <li{if $smarty.foreach.submenu.last} class="lastsub"{/if}><a
                                    href="{$baseurl}{$subname}"{if $subselect==$subitem} class="select"{/if}>{if $smarty.foreach.submenu.last}
                                <span>{/if}{$subitem}{if $smarty.foreach.submenu.last}</span>{/if}</a></li>
                    {/foreach}
                </ul>
            {else}<li class="lastsub"><a href="{$baseurl}{$target}"{if $select==$name} class="select"{/if}>
                <span>{$name}</span></a>{/if}</li>
        {/foreach}
    </ul>
</div>
