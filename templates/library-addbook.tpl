<fieldset>
    <legend>Add Book</legend>
    <form action="{$self}" method="POST">
        <input type="hidden" name="action" value="search"/>

        <div class="note row">Enter an ISBN to attempt to auto-fill book details</div>
        <div class="row">
            <label for="isbn">ISBN</label>
            <span class="textinput"><input type="text" name="isbn"/></span>
        </div>
        <div class="row">
            <span class="textinput"><input type="submit" value="Auto-fill"/></span>
        </div>
    </form>

    {if isset($book.image_url)}
        <div class="emblem">
            <img src="{$book.image_url}"/>
        </div>
    {/if}

    <div>
        <form action="{$self}" method="POST">
            <input type="hidden" name="action" value="add"/>
            <input type="hidden" name="image_url" value="{$book.image_url}"/>

            <div class="row">
                <label for="isbn">ISBN Number</label>
                <span class="textinput"><input type="text" name="isbn" value="{$book.isbn}"/></span>
            </div>

            <div class="row">
                <label for="title">Title</label>
                <span class="textinput"><input type="text" name="title" width="200"
                                               value="{$book.title|escape}"/></span>
            </div>
            <div class="row">
                <label for="author">Author</label>
                <span class="textinput"><input type="text" name="author" value="{$book.author|escape}"/></span>
            </div>
            <div class="row">
                <label for="publisher">Publisher</label>
                <span class="textinput"><input type="text" name="publisher" value="{$book.publisher|escape}"/></span>
            </div>
            <div class="row">
                <label for="comments">Comments</label>
                <span class="textinput"><textarea name="comments"></textarea></span>
            </div>
            <div class="row">
                <input type="submit" name="add" value="Add Book"/>
            </div>
        </form>
    </div>
</fieldset>
